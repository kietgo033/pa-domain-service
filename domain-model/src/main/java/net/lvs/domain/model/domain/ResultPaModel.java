package net.lvs.domain.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 27/04/2022 14:08
 * Phan Hoàng Kiệt
 */
@Data
public class ResultPaModel {

    @JsonProperty(value = "Command")
    @SerializedName(value = "Command")
    String command;
    @JsonProperty(value = "Username")
    @SerializedName(value = "Username")
    String userName;
    @JsonProperty(value = "Money")
    @SerializedName(value = "Money")
    String money;
    @JsonProperty(value = "ReturnCode")
    @SerializedName(value = "ReturnCode")
    int returnCode;
    @JsonProperty(value = "ReturnText")
    @SerializedName(value = "ReturnText")
    String returnText;

    @JsonProperty(value = "Date")
    @SerializedName(value = "Date")
    String date;
    @JsonProperty(value = "Domain")
    @SerializedName(value = "Domain")
    String domain;
}
