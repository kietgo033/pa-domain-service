package net.lvs.domain.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 14:39 22/04/2022
 * Admin
 */

public class DomainINetModel {
    private String id;
    private String orgId;
    private String typeId;
    private String customerId;
    private String name;
    private int period;
    private String domainName;
    private List<HostNameINetModel> nsList;
    private List<ContactINetModel> contacts;
    public String verifyStatus;
    public String registrar;
    public String contract;


    public DomainINetModel() {
        nsList = new ArrayList<>();
        contacts = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<HostNameINetModel> getNsList() {
        return nsList;
    }

    public void setNsList(List<HostNameINetModel> nsList) {
        this.nsList = nsList;
    }

    public List<ContactINetModel> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactINetModel> contacts) {
        this.contacts = contacts;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
}
