package net.lvs.domain.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * 28/04/2022 16:57
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HostNameModel {
    @SerializedName("hostname")
    @JsonProperty(value = "hostname")
    private String hostName;
    @SerializedName("ipv4")
    @JsonProperty(value = "ipv4")
    private String ipv4;
}
