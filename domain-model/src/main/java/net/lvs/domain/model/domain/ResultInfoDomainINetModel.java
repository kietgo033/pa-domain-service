package net.lvs.domain.model.domain;

/**
 * Huy_Nguyen
 * Date: 11/06/2020
 * Time: 9:36 SA
 */
public class ResultInfoDomainINetModel {
    private String id;
    private String name;
    private String suffix;
    private String label;
    private String roidType;
    private String customerId;
    private String status;
    private String issueDate;
    private String expireDate;
    private String customerName;
    private String customerEmail;
    private String verifyStatus;
    private String registrant;
    private String contractToken;
    private String contract;
    private String registrar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRoidType() {
        return roidType;
    }

    public void setRoidType(String roidType) {
        this.roidType = roidType;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getRegistrant() {
        return registrant;
    }

    public void setRegistrant(String registrant) {
        this.registrant = registrant;
    }

    public String getContractToken() {
        return contractToken;
    }

    public void setContractToken(String contractToken) {
        this.contractToken = contractToken;
    }

    public String getContract() { return contract; }

    public void setContract(String contract) { this.contract = contract; }

    public String getRegistrar() { return registrar; }

    public void setRegistrar(String registrar) { this.registrar = registrar; }
}