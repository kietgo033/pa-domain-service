package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 11:24
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InetDNSSECManualDTO {
    private String keyTag;
    private String alg;
    private String digest;
    private String digestType;
    private String domainId;
    private String recordType;
    private String data;
}
