package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.lvs.domain.model.domain.RecordInetModel;

import java.util.List;

/**
 * 04/05/2022 15:36
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InetUpdateRecordDomainDTO {
    private String id;
    private List<RecordInetModel> recordList;
}
