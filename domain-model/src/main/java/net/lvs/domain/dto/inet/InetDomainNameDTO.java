package net.lvs.domain.dto.inet;

import com.google.gson.annotations.SerializedName;

/**
 * 26/04/2022 09:36
 * Phan Hoàng Kiệt
 */

public class InetDomainNameDTO {
    @SerializedName(value = "domainName")
    private String domainname;
    public InetDomainNameDTO(String domainname){
        this.domainname=domainname;
    }
}
