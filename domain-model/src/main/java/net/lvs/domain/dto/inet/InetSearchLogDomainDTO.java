package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 16:50
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InetSearchLogDomainDTO {
    private String domainId;
    private String actionName;
    private String fromCreatedDate;
    private String toCreatedDate;
}
