package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 16:51
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InetUploadIdNumber {
    private String id;
    private String documentType;
    private String url;
}
