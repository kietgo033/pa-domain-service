package net.lvs.domain.dto.inet;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import net.lvs.domain.model.domain.HostNameINetModel;
import net.lvs.domain.model.domain.HostNameModel;

import java.util.List;

/**
 * 28/04/2022 14:11
 * Phan Hoàng Kiệt
 */
@Data
public class InetUpdateDNSChildDTO {
    @JsonProperty(value = "id")
    @SerializedName(value = "id")
    private String id;
    @JsonProperty(value = "hostList")
    @SerializedName(value = "hostList")
    private List<HostNameModel> hostList;
}
