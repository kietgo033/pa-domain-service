package net.lvs.domain.dto.inet;

import lombok.Data;
import net.lvs.domain.model.domain.ContactINetModel;
import net.lvs.domain.model.domain.HostNameINetModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 25/04/2022 16:09
 * Phan Hoàng Kiệt
 */
@Data
public class InetCreateDomainDTO {
    private String name;
    private int period;
    private String customerId;
    private List<HostNameINetModel> nsList;
    private List<ContactINetModel> contacts;
}
