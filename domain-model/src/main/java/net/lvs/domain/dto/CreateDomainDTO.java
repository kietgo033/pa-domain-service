package net.lvs.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.lvs.domain.dto.inet.InetCreateDomainDTO;
import net.lvs.domain.model.domain.ContactINetModel;
import net.lvs.domain.model.domain.DomainPaModel;
import net.lvs.domain.model.domain.HostNameINetModel;

import java.util.List;

/**
 * 29/04/2022 15:06
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateDomainDTO {

    // create domain cua PA
    private DomainPaModel domainPaModel;

    //create domain cua Inet
    private InetCreateDomainDTO inetCreateDomainDTO;

}
