package net.lvs.domain.sdk;

import com.google.gson.reflect.TypeToken;
import net.lvs.domain.dto.CreateDomainDTO;
import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.dto.inet.*;

import net.lvs.domain.model.domain.*;
import net.lvs.domain.sdk.base.BaseClient;
import net.lvs.domain.sdk.exceptions.DomainSDKException;
import net.lvs.domain.wrapper.ResponseWrapper;

import java.util.List;


public class DomainController extends BaseClient {
    public DomainController(String urlService, String token) {
        this.urlService = urlService;
        this.token = token;
    }

    /////////////////---------------DOMAIN-------------/////////////////////////////


    public List<ResultInfoDomainINetModel> searchInfoDomainInet(DomainINetModel domainINetModel) throws DomainSDKException {
        ResponseWrapper<List<ResultInfoDomainINetModel>> wrapper = doPost("inet/search",
                domainINetModel,
                new TypeToken<ResponseWrapper<List<ResultInfoDomainINetModel>>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean checkExistDomain(String domain, String configKey) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doGet("exist" + "?domain=" + domain + "&configKey=" + configKey, null,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

    public ResultDomainModel getInfoDomain(String id,String configKey) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doGet("details/" + id+"?configKey="+configKey,
                null,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean updateDNSDomain(InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO, String configKey) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPut("dns"+"?configKey="+configKey,
                inetUpdateDNSDomainDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean updateDNSDomainChild(InetUpdateDNSChildDTO inetUpdateDNSChildDTO, String configKey) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPut("dns-child"+"?configKey="+configKey,
                inetUpdateDNSChildDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

//    public boolean updateDNSChildDomain(InetUpdateDNSChildDTO inetUpdateDNSChildDTO, String configKey) throws DomainSDKException {
//        ResponseWrapper<Boolean> wrapper = doPost("update-dns-child" + "?configKey=" + configKey,
//                inetUpdateDNSChildDTO,
//                new TypeToken<ResponseWrapper<Boolean>>() {
//                }.getType());
//        return getData(wrapper);
//    }

//    public ResultDomainModel createDomain(CreateDomainDTO createDomainDTO,String configKey) throws DomainSDKException {
//        ResponseWrapper<ResultDomainModel> wrapper = doPost("create"+"?configKey="+configKey,
//                createDomainDTO,
//                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
//                }.getType());
//
//        return getData(wrapper);
//
//    }

    public ResultDomainModel renewDomain(RenewDomainDTO renewDomainDTO,String configKey) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doPost("renew"+"?configKey="+configKey,
                renewDomainDTO,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean privacyProtectionDomainInet(String id) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doGet("privacy-protection/" + id,
                null,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        try {
            return getData(wrapper);
        } catch (Exception e) {
            return false;
        }
    }

    public ResultWhoIsModel checkWhoIsDomainInet(String domainName) throws DomainSDKException {
        ResponseWrapper<ResultWhoIsModel> wrapper = doGet("whois/" + domainName,
                null,
                new TypeToken<ResponseWrapper<ResultWhoIsModel>>() {
                }.getType());
        try {
            return getData(wrapper);
        } catch (Exception e) {
            return null;
        }
    }

    public ContactINetModel createAccountCustomerDomainInet(ContactINetModel contactINetModel) throws DomainSDKException {
        ResponseWrapper<ContactINetModel> wrapper = doPost("customer",
                contactINetModel,
                new TypeToken<ResponseWrapper<ContactINetModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean suspendAccountCustomerDomainInet(String id) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doGet("customer/suspend/" + id,
                null,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean activeAccountCustomerDomainInet(String id) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doGet("customer/active/" + id,
                null,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

    public ContactINetModel getAccountCustomerDomainInetById(String id) throws DomainSDKException {
        ResponseWrapper<ContactINetModel> wrapper = doGet("customer/getbyid/" + id,
                null,
                new TypeToken<ResponseWrapper<ContactINetModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public ContactINetModel getAccountCustomerDomainInetByEmail(String email) throws DomainSDKException {
        ResponseWrapper<ContactINetModel> wrapper = doGet("customer/getbyemail/" + email,
                null,
                new TypeToken<ResponseWrapper<ContactINetModel>>() {
                }.getType());
        return getData(wrapper);
    }


    //---------------------Pa Domain----------------------

    public ResultPaModel getAccountStill() throws DomainSDKException {
        ResponseWrapper<ResultPaModel> wrapper = doGet("pa/account-still",
                null,
                new TypeToken<ResponseWrapper<ResultPaModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public ResultPaModel getAccountTotal() throws DomainSDKException {
        ResponseWrapper<ResultPaModel> wrapper = doGet("pa/account-total",
                null,
                new TypeToken<ResponseWrapper<ResultPaModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public ResultPaModel getDateDomainPa(String domain) throws DomainSDKException {
        ResponseWrapper<ResultPaModel> wrapper = doGet("pa/date" + "?domain=" + domain,
                null,
                new TypeToken<ResponseWrapper<ResultPaModel>>() {
                }.getType());
        return getData(wrapper);
    }
    public int checkDomainPa(String domain) throws DomainSDKException {
        ResponseWrapper<Integer> wrapper = doGet("pa/check-domain" + "?domain=" + domain,
                null,
                new TypeToken<ResponseWrapper<Integer>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean changePassword(InetChangePasswordDTO inetChangePasswordDTO, String configKey) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPut("password" + "?configKey=" + configKey,
                inetChangePasswordDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }
    public ResultDomainModel createDomain(CreateDomainDTO createDomainDTO, String configKey) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doPost( "?configKey=" + configKey,
                createDomainDTO,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public boolean updateDomainPa(DomainPaModel domainPaModel) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPut("pa",
                domainPaModel,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }



    //////////-------DNSSEC domain--------///////////
    //Chua test

    public boolean enableDNSSEC(InetIdDTO inetIdDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/enable-dnssec",
                inetIdDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }
    public boolean syncDNSSEC(InetIdDTO inetIdDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/sync-dnssec",
                inetIdDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }
    public boolean disableDNSSEC(InetIdDTO inetIdDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/disable-dnssec",
                inetIdDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }

    /////////////////--------manual----------/////////////////

    public boolean createdDNSSECManual(InetDNSSECManualDTO inetDNSSECManualDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/create-dnssec-manual",
                inetDNSSECManualDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }
    public boolean updateDNSSECManual(InetDNSSECManualDTO inetDNSSECManualDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/update-dnssec-manual",
                inetDNSSECManualDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }
    public boolean deleteDNSSECManual(InetIdDTO inetIdDTO) throws DomainSDKException {
        ResponseWrapper<Boolean> wrapper = doPost("inet/delete-dnssec-manual",
                inetIdDTO,
                new TypeToken<ResponseWrapper<Boolean>>() {
                }.getType());
        return getData(wrapper);
    }





    ////////////------------Inet--------------///
    public ResultDomainModel getRecordDomainInet(String id) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doGet("inet/records/"+id,
                null,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }
    public ResultDomainModel updateRecordDomainInet(InetUpdateRecordDomainDTO inetUpdateRecordDomainDTO) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doPut("inet/records",
                inetUpdateRecordDomainDTO,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public ResultDomainModel resendEmailDomainInet(String id) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doPost("inet/resend-email"+"?id="+id,
                null,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }

    public ResultDomainModel changeAuthCodeDomainInet(InetChangeAuthCodeDTO inetChangeAuthCodeDTO) throws DomainSDKException {
        ResponseWrapper<ResultDomainModel> wrapper = doPost("inet/authcode",
                inetChangeAuthCodeDTO,
                new TypeToken<ResponseWrapper<ResultDomainModel>>() {
                }.getType());
        return getData(wrapper);
    }





}