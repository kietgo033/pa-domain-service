package net.lvs.domain.handler;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DomainServiceException extends ResponseStatusException {

    @Getter
    private Object data;


    public DomainServiceException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }

    public DomainServiceException(Object data, String message) {
        super(HttpStatus.NOT_FOUND,message);
        this.data = data;
    }
}
