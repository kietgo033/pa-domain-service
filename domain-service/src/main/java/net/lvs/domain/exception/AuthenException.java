package net.lvs.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class AuthenException extends Exception {

    public AuthenException(String message) {
        super(message);
    }

    public AuthenException(String message, Throwable cause) {
        super(message, cause);
    }
}
