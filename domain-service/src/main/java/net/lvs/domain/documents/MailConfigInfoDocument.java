package net.lvs.domain.documents;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document("Axigen")
@Data
public class MailConfigInfoDocument {
    @Id
    private String _id;
    private String id;
    private Date createTime;
    private String orgId;
    private String key;
    private String province;
    private String webMail;
    private String webAdmin;
    @Field("userAdmin")
    private String user;
    @Field("passwordAdmin")
    private String password;
    private String urlTelnet;
    private String ip;
    private String smtp;
    private String pop3;
    private String imap;
    private String axigenPath;
    private Boolean enableSupport;
}
