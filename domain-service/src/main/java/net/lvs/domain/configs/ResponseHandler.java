package net.lvs.domain.configs;

import net.lvs.domain.wrapper.ObjectResponseWrapper;
import org.springframework.core.MethodParameter;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.LinkedHashMap;
import java.util.TreeMap;


@RestControllerAdvice
public class ResponseHandler implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object data, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // check bypass swagger
//        String requestPath=((ServletServerHttpRequest) serverHttpRequest).getServletRequest().getRequestURL().toString();
//        if(requestPath.contains("/swagger-resources")) return data;
//        if (data instanceof UiConfiguration) return data;
//        if (data instanceof SecurityConfiguration) return data;


        if (data instanceof String && ((String) data).contains("openapi")) return data;
        if (data instanceof Resource) return data;
        if (data instanceof ObjectResponseWrapper) return data;
        if (data instanceof TreeMap) return data;
        if (data instanceof LinkedHashMap) return data;
        return ObjectResponseWrapper.builder().status(1).message("").data(data).build();
    }
}
