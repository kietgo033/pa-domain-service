package net.lvs.domain.service.inet;

import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.dto.inet.*;
import net.lvs.domain.model.domain.*;

import java.util.List;

public interface IDomainInetService {
    ResultDomainModel createDomainInet(InetCreateDomainDTO inetCreateDomainDTO) throws Exception;

    ResultDomainModel renewDomainInet(RenewDomainDTO renewDomainDTO) throws Exception;

    List<ResultInfoDomainINetModel> searchDomainInet(DomainINetModel domainINetModel) throws Exception;

    boolean checkExistDomainInet(String domain) throws Exception;

    ResultDomainModel getInfoDomainInet(String id) throws Exception;

    boolean updateDNSDomainInet(InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO) throws Exception;

    boolean updateDNSChildDomainInet(InetUpdateDNSChildDTO inetUpdateDNSChildDTO) throws Exception;

    boolean privacyProtectionDomainInet(String id) throws Exception;

    ResultWhoIsModel checkWhoIsDomainInet(String domainName) throws Exception;

    boolean changePasswordDomainInet(InetChangePasswordDTO inetChangePasswordDTO) throws Exception;

    boolean enableDNSSEC(String id) throws Exception;

    boolean syncDNSSEC(String id) throws Exception;

    boolean disableDNSSEC(String id) throws Exception;

    boolean createDNSSECManual(InetDNSSECManualDTO inetDnssecManualDTO) throws Exception;

    boolean updateDNSSECManual(InetDNSSECManualDTO inetDnssecManualDTO) throws Exception;

    boolean deleteDNSSECManual(String id) throws Exception;

    ResultDomainModel getRecordDomainInet(String id) throws Exception;

    boolean updateRecordDomainInet(InetUpdateRecordDomainDTO inetUpdateRecordDomainDTO) throws Exception;

    boolean resendEmailVerificationDomainInet(String id) throws Exception;

    boolean changeAuthcodeDomainInet(InetChangeAuthCodeDTO inetChangeAuthCodeDTO) throws Exception;

    ResultDomainModel searchLogDomainInet(InetSearchLogDomainDTO inetSearchLogDomainDTO) throws Exception;

    boolean uploadContractDomainInet(InetUploadContractDTO inetUploadContractDTO) throws Exception;

    boolean uploadIdNumberDomainInet(InetUploadIdNumber inetUploadIdNumber) throws Exception;


}