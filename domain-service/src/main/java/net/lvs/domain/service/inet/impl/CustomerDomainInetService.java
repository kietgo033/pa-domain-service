package net.lvs.domain.service.inet.impl;

import net.lvs.domain.service.BaseDomainInetAPI;
import net.lvs.domain.dto.inet.InetEmailDTO;
import net.lvs.domain.dto.inet.InetIdDTO;
import net.lvs.domain.model.domain.ContactINetModel;
import net.lvs.domain.model.domain.ResultDomainModel;
import net.lvs.domain.service.inet.ICustomerDomainInetService;
import net.lvs.domain.utils.CommandInetUtils;
import org.springframework.stereotype.Service;

/**
 * 26/04/2022 13:27
 * Phan Hoàng Kiệt
 */
@Service
public class CustomerDomainInetService extends BaseDomainInetAPI implements ICustomerDomainInetService {
    @Override
    public ContactINetModel creatAccount(ContactINetModel contactINetModel) throws Exception {
        String result = resultINet(contactINetModel, CommandInetUtils.CREATE_ACCOUNT);
        ContactINetModel rs = resultContact(result);
        if (rs.getStatus() != null)
            if (rs.getStatus().equals(ResultDomainModel.ERROR))
                throw new Exception(rs.getMessage());
        return rs;
    }

    @Override
    public ContactINetModel getAccountById(String id) throws Exception {
        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.GET_ACCOUNT_BY_ID);
        ContactINetModel rs = resultContact(result);
        if (rs.getStatus() != null)
            if (rs.getStatus().equals(ResultDomainModel.ERROR))
                throw new Exception(rs.getMessage());
        return rs;
    }

    @Override
    public ContactINetModel getAccountByEmail(String email) throws Exception {
        InetEmailDTO inetEmailDTO = new InetEmailDTO(email);
        String result = resultINet(inetEmailDTO, CommandInetUtils.GET_ACCOUNT_BY_EMAIL);
        ContactINetModel rs = resultContact(result);
        if (rs == null)
            throw new Exception("Không tìm thấy thông tin khách hàng với email:  "+email);
        return rs;
    }

    @Override
    public boolean suspendAccount(String id) throws Exception {

        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.SUSPEND_ACCOUNT);
        ContactINetModel rs = resultContact(result);
        if (rs.getStatus() != null)
            if (rs.getStatus().equals(ResultDomainModel.ERROR))
                throw new Exception(rs.getMessage());
        if (rs.getId().equals(id))
            return true;
        return false;
    }

    @Override
    public boolean activeAccount(String id) throws Exception {
        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.ACTIVE_ACCOUNT);
        ContactINetModel rs = resultContact(result);
        if (rs.getStatus() != null)
            if (rs.getStatus().equals(ResultDomainModel.ERROR))
                throw new Exception(rs.getMessage());
        if (rs.getId().equals(id))
            return true;
        return false;
    }
}
