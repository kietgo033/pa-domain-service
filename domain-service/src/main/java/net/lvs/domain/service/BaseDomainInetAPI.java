package net.lvs.domain.service;

import com.google.common.net.HttpHeaders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.deploy.net.HttpUtils;
import net.lvs.domain.model.domain.ContactINetModel;
import net.lvs.domain.model.domain.ResultDomainModel;
import net.lvs.domain.model.domain.ResultInfoDomainINetModel;
import net.lvs.domain.model.domain.ResultWhoIsModel;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;


import javax.net.ssl.*;
import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 14:12 22/04/2022
 * Admin
 */

public class BaseDomainInetAPI {

    protected String tokenDomainInet = "0F6C70B3570B24E96B70733F73D6D082C7F77EA4";
    protected String urlDomainInet = "https://dms.inet.vn";
    public ModelMapper mapper = new ModelMapper();
    private static Logger logger = Logger.getLogger(BaseDomainInetAPI.class.getName());

    public String resultINet(Object model, String command) {
        try {
            Gson gson = new Gson();
            String strJsonBody = null;

            strJsonBody = gson.toJson(model);

            String request = urlDomainInet + command;
            URL url = new URL(urlDomainInet + command);
            System.out.println(request);
            trustSSL();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            String token = tokenDomainInet;
            conn.setRequestProperty("token", token);
            conn.setRequestMethod("POST");
            conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, String.valueOf(MediaType.APPLICATION_JSON));
            conn.setDoOutput(true);

            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(strJsonBody.getBytes());
            outputStream.flush();
            int httpResponse = conn.getResponseCode();
            if (httpResponse == 200) {
                String result = readInputStreamToString(conn);
                logger.info(result);
                return result;
            } else {
                logger.info("Error code " + httpResponse);
            }

        }catch (Exception e){
            logger.info(e.getMessage());
        }
        return null;
    }


    public static ResultDomainModel resultDomain(String result) {
        Gson gson = new Gson();
        Type type = new TypeToken<ResultDomainModel>() {
        }.getType();
        ResultDomainModel resultDomainModel = gson.fromJson(result, type);
        if (null != resultDomainModel) {
            return resultDomainModel;
        }
        return null;
    }

    public static ResultWhoIsModel resultWhoIs(String result) {
        Gson gson = new Gson();
        Type type = new TypeToken<ResultWhoIsModel>() {
        }.getType();
        ResultWhoIsModel resultWhoIsModel = gson.fromJson(result, type);
        if (null != resultWhoIsModel) {
            return resultWhoIsModel;
        }
        return null;
    }

    public static ContactINetModel resultContact(String result) {
        Gson gson = new Gson();
        Type type = new TypeToken<ContactINetModel>() {
        }.getType();
        ContactINetModel contactINetModel = gson.fromJson(result, type);
        if (null != contactINetModel) {
            return contactINetModel;
        }
        return null;
    }


    //


    public String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        } catch (Exception e) {
            result = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
        return result;
    }

    public static List<ResultInfoDomainINetModel> resultInfo(String result) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<ResultInfoDomainINetModel>>() {
        }.getType();
        return gson.fromJson(result, type);
    }

    public static void trustSSL() {  // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            Logger.getLogger(HttpUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        HostnameVerifier allHostsValid = (String hostname, SSLSession session) -> true;

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }


}
