package net.lvs.domain.service.inet;

import net.lvs.domain.model.domain.ContactINetModel;

public interface ICustomerDomainInetService {

    ContactINetModel creatAccount(ContactINetModel contactINetModel) throws Exception;
    ContactINetModel getAccountById(String id) throws Exception;
    ContactINetModel getAccountByEmail(String email) throws Exception;
    boolean suspendAccount(String id) throws Exception;
    boolean activeAccount(String id) throws Exception;
}
