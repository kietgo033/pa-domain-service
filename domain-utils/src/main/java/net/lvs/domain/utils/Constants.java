package net.lvs.domain.utils;

public class Constants {
    public static final String API_VERSION = "v1";
    public static final String URL_SERVICE = API_VERSION + "/domain";
    public static final String HEADER_TOKEN_API = "";

//    public static final String URL_SERVICE_MAIL_DOMAIN = "/domain";
//    public static final String URL_SERVICE_MAIL_DELETE_DOMAIN = "/domain/{domain}";
//    public static final String URL_SERVICE_MAIL_CHANGE_NAME_DOMAIN = "/domain/change-name";
//    public static final String URL_SERVICE_MAIL_GET_NAME_DOMAIN = "/domain/name";
//    public static final String URL_SERVICE_MAIL_CHECK_EXISTS_DOMAIN = "/domain/{domain}/existed";
//    public static final String URL_SERVICE_MAIL_GET_STATUS_DOMAIN = "/domain/{domain}/status";
//    public static final String URL_SERVICE_MAIL_CHANGE_STATUS_DOMAIN = "/domain/{domain}/status/{status}";
//    public static final String URL_SERVICE_MAIL_GET_STORAGE_DOMAIN = "domain/storage/use-all";
//    public static final String URL_SERVICE_MAIL_GET_DOMAINS_BY_RESOURCE = "/domain/get-by-resources"; // thieu "s"
//    public static final String URL_SERVICE_MAIL_GET_DOMAINS_BY_DOMAINS= "/domain/get-by-domains";
//    public static final String URL_SERVICE_MAIL_UPDATE_LIMIT_ACCOUNT = "/domain/account/limit";
//    public static final String URL_SERVICE_MAIL_GET_ACCOUNTS_BY_DOMAIN = "/domain/{domain}/account";
//    public static final String URL_SERVICE_MAIL_GET_ACCOUNT_BY_DOMAIN = "/domain/{domain}/account/{account}";
//    public static final String URL_SERVICE_MAIL_ADD_MESSAGE = "/domain/{domain}/add-localtion";
//    public static final String URL_SERVICE_MAIL_GET_GROUPS_BY_DOMAIN = "/domain/{domain}/group";
//    public static final String URL_SERVICE_MAIL_ADD_GROUP = "/domain/{domain}/group";
//    public static final String URL_SERVICE_MAIL_DELETE_GROUP = "/domain/{domain}/group/{group}";
//    public static final String URL_SERVICE_MAIL_GET_ACCOUNT_BY_GROUP = "/domain/{domain}/group/{group}/member";
//    public static final String URL_SERVICE_MAIL_DELETE_ACCOUNT_BY_GROUP = "/domain/{domain}/group/{group}/member/{member}";
//    public static final String URL_SERVICE_MAIL_ADD_ACCOUNTS_FOR_GROUP = "/domain/{domain}/group/member";
//    //chinh sua user -> account, member
//    public static final String URL_SERVICE_MAIL_CREATE_ACCOUNT = "/domain/{domain}/account";
//    public static final String URL_SERVICE_MAIL_DELETE_ACCOUNT = "/domain/{domain}/account/{account}";
//    public static final String URL_SERVICE_MAIL_CHANGE_PW_ADMIN = "/change-admin-password";
//    public static final String URL_SERVICE_MAIL_CHANGE_NAME_ACCOUNT = "/domain/{domain}/account/change-fullname";
//    public static final String URL_SERVICE_MAIL_CHANG_PW_ACCOUNT = "/domain/{domain}/account/change-password";

}
